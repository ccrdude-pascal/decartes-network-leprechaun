unit NetworkLeprechaun.Wireguard.FrameStatusWG;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Firefly.Process,
   NetworkLeprechaun.FrameStatus;

type

   { TFrameStatusWireguard }

   TFrameStatusWireguard = class(TFrameStatus)
   private

   public
      function StatusWorking: boolean; override;
      procedure UpdateStatus; override;
      procedure TakeAction; override;
   end;

implementation

{$R *.lfm}

uses
   Dialogs;

   { TFrameStatusWireguard }

function TFrameStatusWireguard.StatusWorking: boolean;
begin
   Result := FileExists('/usr/local/bin/wg-quick');
end;

procedure TFrameStatusWireguard.UpdateStatus;
begin
   inherited;
   if StatusWorking then begin
      labelStatus.Caption := 'Wireguard installed.';
   end else begin
      labelStatus.Caption := 'Wireguard missing!';
      bnAction.Caption := 'Install Wireguard CLI';
   end;
end;

procedure TFrameStatusWireguard.TakeAction;
var
   sOutput: string;
begin
   if RunLoggedCommand('/usr/local/bin/brew', ['install', 'wireguard-tools'], sOutput) then begin
      MessageDlg('Wireguard CLI Installation', sOutput, mtInformation, [mbOK], 0);
   end else begin
      MessageDlg('Wireguard CLI Installation', 'Installation failed. Try the following in Terminal:' + #13#10 + 'brew install wireguard-tools', mtError, [mbOK], 0);
   end;
end;

end.
