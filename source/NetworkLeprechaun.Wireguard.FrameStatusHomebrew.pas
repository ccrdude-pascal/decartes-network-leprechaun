unit NetworkLeprechaun.Wireguard.FrameStatusHomebrew;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   NetworkLeprechaun.FrameStatus;

type

   { TFrameStatusHomebrew }

   TFrameStatusHomebrew = class(TFrameStatus)
   private
   public
      function StatusWorking: boolean; override;
      procedure UpdateStatus; override;
      procedure TakeAction; override;
   end;

implementation

{$R *.lfm}

uses
   LCLIntf;

{ TFrameStatusHomebrew }

function TFrameStatusHomebrew.StatusWorking: boolean;
begin
   Result := FileExists('/usr/local/bin/brew');
end;

procedure TFrameStatusHomebrew.UpdateStatus;
begin
   inherited;
   if StatusWorking then begin
      labelStatus.Caption := 'Homebrew installed.';
   end else begin
      labelStatus.Caption := 'Homebrew missing!';
      bnAction.Caption := 'Install Homebrew';
   end;
end;

procedure TFrameStatusHomebrew.TakeAction;
begin
   OpenURL('https://brew.sh/');
end;

end.
