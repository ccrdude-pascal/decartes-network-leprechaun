unit NetworkLeprechaun.UserMounts.FrameLibFuse;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   NetworkLeprechaun.FrameStatus;

type

   { TFrameStatusLibFuse }

   TFrameStatusLibFuse = class(TFrameStatus)
   private

   public
      function StatusWorking: boolean; override;
      procedure UpdateStatus; override;
      procedure TakeAction; override;
   end;

implementation

{$R *.lfm}

uses
   Dialogs,
   Firefly.Process;

{ TFrameStatusLibFuse }

function TFrameStatusLibFuse.StatusWorking: boolean;
begin
   Result := FileExists('/usr/local/lib/libfuse.dylib');
end;

procedure TFrameStatusLibFuse.UpdateStatus;
begin
   inherited UpdateStatus;
  if StatusWorking then begin
     labelStatus.Caption := 'LibFuse installed.';
  end else begin
     labelStatus.Caption := 'LibFuse missing!';
     bnAction.Caption := 'Install LibFuse';
  end;
end;

procedure TFrameStatusLibFuse.TakeAction;
var
   sOutput: string;
begin
   if RunLoggedCommand('/usr/local/bin/brew', ['install', 'macfuse'], sOutput) then begin
      MessageDlg('MacFuse Installation', sOutput, mtInformation, [mbOK], 0);
   end else begin
      MessageDlg('MacFuse Installation', 'Installation failed. Try the following in Terminal:' + #13#10 + 'brew install macfuse', mtError, [mbOK], 0);
   end;
end;

end.

