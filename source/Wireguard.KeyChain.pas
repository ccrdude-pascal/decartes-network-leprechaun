{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Access to macOS keychain WireGuad items.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-24  pk   2h  Wrote code to retrieve WireGuard entries.
// *****************************************************************************
   )
}

unit Wireguard.KeyChain;

{$mode Delphi}{$H+}

{$IFDEF Darwin}
{$linkframework Security}
{$modeswitch objectivec1 on}
{$ENDIF Darwin}

interface

uses
   Classes,
   SysUtils,
   MacOSAll,
   Firefly.MacOS.KeyChain;

{
  Returns the first found Wireguard profile.

  @see TWireguardProfilesInKeyChain
}
function GetFirstWireguardProfile(out AProfile: ansistring): boolean;
{
  Returns all Wireguard profiles.

  @see TWireguardProfilesInKeyChain
}
function GetWireguardProfiles(constref AList: TStrings): boolean;

type

   { TWireguardProfilesInKeyChain }

   TWireguardProfilesInKeyChain = class(TKeyChainSearch)
   public
      function List: boolean;
   end;

implementation

procedure WireguardKeyChainDebugLine(ALine: string);
begin
   WriteLn(ALine);
end;

function GetFirstWireguardProfile(out AProfile: ansistring): boolean;
var
   s: OSStatus;
   iMaxLen: uint32;
   iActualLen: uint32;
   pc: pansichar;
   item: KCItemRefPtr;
   str: Str255;
begin
   //   function KCFindGenericPassword( serviceName: ConstStringPtr { can be NULL }; accountName: ConstStringPtr { can be NULL }; maxLength: UInt32; passwordData: UnivPtr; var actualLength: UInt32; item: KCItemRefPtr { can be NULL } ): OSStatus; external name '_KCFindGenericPassword';
   // @see https://developer.apple.com/documentation/coreservices/1562994-kcfindgenericpassword
   str := 'com.wireguard.macos';
   iMaxLen := 1024;
   iActualLen := 0;
   pc := AllocMem(iMaxLen);
   try
      item := nil;
      s := KCFindGenericPassword(@str, nil, iMaxLen, pc, iActualLen, item);
      Result := (0 = s);
      if Result then begin
         AProfile := pc;
      end;
   finally
      FreeMem(pc);
   end;
end;

function GetWireguardProfiles(constref AList: TStrings): boolean;
var
   kcs: TWireguardProfilesInKeyChain;
   e: TKeyChainEntry;
begin
   kcs := TWireguardProfilesInKeyChain.Create(True);
   try
      Result := kcs.List;
      for e in kcs do begin
         AList.Add(e.Data);
      end;
   finally
      kcs.Free;
   end;
end;

{ TWireguardProfilesInKeyChain }

function TWireguardProfilesInKeyChain.List: boolean;
begin
   Result := FindGenericPasswordsForService('com.wireguard.macos', true);
end;

end.
