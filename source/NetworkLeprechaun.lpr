program NetworkLeprechaun;

{$mode objfpc}{$H+}

uses
   {$IFDEF UNIX}
  cthreads,
   {$ENDIF}
   {$IFDEF HASAMIGA}
  athreads,
   {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms, lazcontrols,
   NetworkLeprechaun.FormMain;

   {$R *.res}

begin
   RequireDerivedFormResource := True;
   Application.Title:='Network Leprechaun';
   Application.Scaled:=True;
   Application.Initialize;
   Application.CreateForm(TFormNetworkLeprechaun, FormNetworkLeprechaun);
   Application.Run;
end.
