unit NetworkLeprechaun.FrameStatus;

{$mode ObjFPC}{$H+}

interface

uses
   Classes, SysUtils, Forms, Controls, StdCtrls, ExtCtrls, Graphics;

type

   { TFrameStatus }

   TFrameStatus = class(TFrame)
      bnAction: TButton;
      ilStandard: TImageList;
      imgStatus: TImage;
      labelStatus: TLabel;
      panelMain: TPanel;
      panelRight: TPanel;
      procedure bnActionClick(Sender: TObject);
   private

   public
      function StatusWorking: boolean; virtual;
      procedure UpdateStatus; virtual;
      procedure TakeAction; virtual;
   end;

implementation

{$R *.lfm}

{ TFrameStatus }

procedure TFrameStatus.bnActionClick(Sender: TObject);
begin
   TakeAction;
end;

function TFrameStatus.StatusWorking: boolean;
begin
   Result := False;
end;

procedure TFrameStatus.UpdateStatus;
begin
   if StatusWorking then begin
      bnAction.Visible := False;
      labelStatus.Font.Style := [];
      ilStandard.GetBitmap(0, imgStatus.Picture.Bitmap);
   end else begin
      bnAction.Visible := True;
      labelStatus.Font.Style := [fsBold];
      ilStandard.GetBitmap(1, imgStatus.Picture.Bitmap);
   end;
end;

procedure TFrameStatus.TakeAction;
begin
   UpdateStatus;
end;

end.
