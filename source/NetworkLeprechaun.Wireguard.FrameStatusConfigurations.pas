unit NetworkLeprechaun.Wireguard.FrameStatusConfigurations;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Wireguard.Connections,
   NetworkLeprechaun.FrameStatus;

type

   { TFrameStatusWireguardConfigurations }

   TFrameStatusWireguardConfigurations = class(TFrameStatus)
   private
      FConnections: TWireguardConnectionList;

   public
      function StatusWorking: boolean; override;
      procedure UpdateStatus; override;
      procedure TakeAction; override;
      property Connections: TWireguardConnectionList read FConnections write FConnections;
   end;

implementation

{$R *.lfm}

uses
   IniFiles,
   Dialogs,
   Firefly.MacOS.KeyChain,
   Wireguard.KeyChain;

   { TFrameStatusWireguardConfigurations }

function TFrameStatusWireguardConfigurations.StatusWorking: boolean;
begin
   Result := False;
   if not Assigned(FConnections) then begin
      Exit;
   end;
   if 0 = FConnections.Count then begin
      Exit;
   end;
   Result := True;
end;

procedure TFrameStatusWireguardConfigurations.UpdateStatus;
begin
   inherited;
   if StatusWorking then begin
      labelStatus.Caption := 'Configurations available.';
   end else begin
      labelStatus.Caption := 'Configurations missing!';
      bnAction.Caption := 'Import from Wireguard GUI';
   end;
end;

procedure TFrameStatusWireguardConfigurations.TakeAction;
var
   kcs: TWireguardProfilesInKeyChain;
   e: TKeyChainEntry;
   ss: TStringStream;
   mif: TMemIniFile;
   sPeerEndPoint: string;
   sPeerPublicKey: string;
   bExisting: boolean;
   conn: TWireguardConnection;
   sName: string;
   sTunnel: string;
begin
   if not Assigned(FConnections) then begin
      Exit;
   end;
   kcs := TWireguardProfilesInKeyChain.Create(True);
   try
      kcs.List;
      for e in kcs do begin
         ss := TStringStream.Create(e.Data);
         try
            mif := TMemIniFile.Create(ss);
            try
               sPeerEndPoint := mif.ReadString('Peer', 'Endpoint', '');
               sPeerPublicKey := mif.ReadString('Peer', 'PublicKey', '');
               bExisting := FConnections.Find(sPeerEndPoint, sPeerPublicKey, conn);
               if (not bExisting) then begin
                  sTunnel := e.ItemLabel;
                  sTunnel := StringReplace(sTunnel, 'WireGuard Tunnel: ', '', [rfIgnoreCase]);
                  sName := LowerCase(sTunnel);
                  sName := StringReplace(sName, '.', '', [rfReplaceAll]);
                  sName := StringReplace(sName, ' ', '', [rfReplaceAll]);
                  if InputQuery('Import Wireguard tunnel', Format('Please enter a short name (no spaces or special characters) for the Wireguard tunnel named "%s".', [sTunnel]), sName) then begin
                     ss.SaveToFile('/usr/local/etc/wireguard/' + sName + '.conf');
                  end;
               end;
            finally
               mif.Free;
            end;
         finally
           ss.Free;
         end;
      end;
   finally
      kcs.Free;
   end;
end;

end.
