unit NetworkLeprechaun.UserMounts.FormConfig;

{$mode ObjFPC}{$H+}

interface

uses
   Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ButtonPanel,
   ShortPathEdit, Firefly.MacOS.UserMounts;

type

   { TFormUserMountEdit }

   TFormUserMountEdit = class(TForm)
      ButtonPanel1: TButtonPanel;
      cbKeyFilename: TComboBox;
      editUsername: TEdit;
      editRemotePath: TEdit;
      editLocalPath: TEdit;
      editVolume: TEdit;
      editHost: TEdit;
      labelLocalPath: TLabel;
      labelUsername: TLabel;
      labelRemotePath: TLabel;
      labelKeyFilename: TLabel;
      labelVolume: TLabel;
      labelHost: TLabel;
      procedure editLocalPathKeyDown({%H-}Sender: TObject; var {%H-}Key: word; {%H-}Shift: TShiftState);
      procedure editVolumeChange({%H-}Sender: TObject);
      procedure editVolumeKeyDown({%H-}Sender: TObject; var {%H-}Key: word; {%H-}Shift: TShiftState);
   private
      function GetDefaultFolderName: string;
      procedure LoadFrom(AConfig: TMacOSMountPoint);
      procedure SaveTo(AConfig: TMacOSMountPoint);
      procedure FillSSHKeys(ADefault: string = '');
   public
      function Add(AConfig: TMacOSMountPoint): boolean;
      function Edit(AConfig: TMacOSMountPoint): boolean;
   end;


function AddUserMountConfig(AConfig: TMacOSMountPoint): boolean;
function EditUserMountConfig(AConfig: TMacOSMountPoint): boolean;

implementation

function AddUserMountConfig(AConfig: TMacOSMountPoint): boolean;
var
   form: TFormUserMountEdit;
begin
   form := TFormUserMountEdit.Create(nil);
   try
      Result := form.Add(AConfig);
   finally
      form.Free;
   end;
end;

function EditUserMountConfig(AConfig: TMacOSMountPoint): boolean;
var
   form: TFormUserMountEdit;
begin
   form := TFormUserMountEdit.Create(nil);
   try
      Result := form.Edit(AConfig);
   finally
      form.Free;
   end;
end;

{$R *.lfm}

{ TFormUserMountEdit }

procedure TFormUserMountEdit.editVolumeChange(Sender: TObject);
begin
   ButtonPanel1.OKButton.Enabled := (Length(editVolume.Text) > 0);
   if (1 = editLocalPath.Tag) then begin
      editLocalPath.Text := Self.GetDefaultFolderName;
   end;
end;

procedure TFormUserMountEdit.editVolumeKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
begin
   if (1 = editLocalPath.Tag) then begin
      editLocalPath.Text := Self.GetDefaultFolderName;
   end;
end;

procedure TFormUserMountEdit.editLocalPathKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
begin
   editLocalPath.Tag := 0;
end;

function TFormUserMountEdit.GetDefaultFolderName: string;
begin
   Result := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(GetUserDir) + 'Volumes') + editVolume.Text;
end;

procedure TFormUserMountEdit.LoadFrom(AConfig: TMacOSMountPoint);
begin
   FillSSHKeys(Aconfig.KeyFilename);
   editUsername.Text := AConfig.RemoteUsername;
   editRemotePath.Text := AConfig.RemotePath;
   editVolume.Text := AConfig.VolumeName;
   editHost.Text := AConfig.RemoteHost;
   editLocalPath.Text := AConfig.FolderName;
   if (GetDefaultFolderName = AConfig.FolderName) then begin
      editLocalPath.Tag := 1;
   end else begin
      editLocalPath.Tag := 0;
   end;
end;

procedure TFormUserMountEdit.SaveTo(AConfig: TMacOSMountPoint);
begin
   AConfig.RemoteUsername := editUsername.Text;
   AConfig.RemotePath := editRemotePath.Text;
   AConfig.VolumeName := editVolume.Text;
   AConfig.RemoteHost := editHost.Text;
   AConfig.FolderName := editLocalPath.Text;
   AConfig.KeyFilename := cbKeyFilename.Text;
end;

procedure TFormUserMountEdit.FillSSHKeys(ADefault: string);
var
   i: integer;
   sr: TSearchRec;
   sPath: string;
begin
   cbKeyFilename.Items.Clear;
   sPath := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(GetUserDir) + '.ssh');
   i := FindFirst(sPath + '*', faAnyFile, sr);
   try
      while (i = 0) do begin
         if ('.' <> sr.Name) and ('..' <> sr.Name) then begin
            cbKeyFilename.Items.Add(sPath + sr.Name);
         end;
         i := FindNext(sr);
      end;
   finally
      FindClose(sr);
   end;
   i := cbKeyFilename.Items.IndexOf(ADefault);
   if i < 0 then begin
      i := cbKeyFilename.Items.Add(ADefault);
   end;
   cbKeyFilename.ItemIndex := i;
end;

function TFormUserMountEdit.Add(AConfig: TMacOSMountPoint): boolean;
begin
   ButtonPanel1.OKButton.Enabled := False;
   Result := (mrOk = ShowModal);
   if Result then begin
      SaveTo(AConfig);
   end;
end;

function TFormUserMountEdit.Edit(AConfig: TMacOSMountPoint): boolean;
begin
   ButtonPanel1.OKButton.Enabled := True;
   LoadFrom(AConfig);
   Result := (mrOk = ShowModal);
   if Result then begin
      SaveTo(AConfig);
   end;
end;

end.
