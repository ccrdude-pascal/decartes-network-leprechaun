unit NetworkLeprechaun.FrameTests;

{$mode ObjFPC}{$H+}

interface

uses
   Classes, SysUtils, Forms, Controls, StdCtrls, ComboEx, SynEdit, Process;

type

   { TFrameTerminalCommandOutput }

   TFrameTerminalCommandOutput = class(TFrame)
      cbCommands: TComboBoxEx;
      mOutput: TMemo;
      procedure cbCommandsChange(Sender: TObject);
      procedure cbCommandsSelect(Sender: TObject);
   private
      FOutputs: TStringList;
   public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure DisplayOutput(ACommand: TProcessString; AParameters: array of TProcessString; AnOutput: TProcessString);
      procedure DisplayOutputEx(const AnExecutableName: TProcessString; const TheParameters: array of TProcessString;
   const TheOutput, TheErrors: string; TheExitStatus: integer);
   end;

implementation

{$R *.lfm}

{ TFrameTerminalCommandOutput }

procedure TFrameTerminalCommandOutput.cbCommandsChange(Sender: TObject);
begin
end;

procedure TFrameTerminalCommandOutput.cbCommandsSelect(Sender: TObject);
begin
   mOutput.Lines.Text := FOutputs[cbCommands.ItemIndex];
   mOutput.Lines.Insert(0, cbCommands.ItemsEx[cbCommands.ItemIndex].Caption);
end;

constructor TFrameTerminalCommandOutput.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
   FOutputs := TStringList.Create;
end;

destructor TFrameTerminalCommandOutput.Destroy;
begin
   FOutputs.Free;
   inherited Destroy;
end;

procedure TFrameTerminalCommandOutput.DisplayOutput(ACommand: TProcessString; AParameters: array of TProcessString; AnOutput: TProcessString);
var
   sCommand: string;
   s: string;
   item: TComboExItem;
begin
   sCommand := ACommand;
   for s in AParameters do begin
      sCommand += ' ' + s;
   end;
   item := cbCommands.ItemsEx.Insert(0);
   item.Caption := sCommand;
   FOutputs.Insert(0, AnOutput);
   cbCommands.ItemIndex := item.Index;
   mOutput.Lines.Text := AnOutput;
   mOutput.Lines.Insert(0, sCommand);
end;

procedure TFrameTerminalCommandOutput.DisplayOutputEx(
   const AnExecutableName: TProcessString;
   const TheParameters: array of TProcessString; const TheOutput,
   TheErrors: string; TheExitStatus: integer);
var
   sCommand: string;
   s: string;
   item: TComboExItem;
begin
   sCommand := AnExecutableName;
   for s in TheParameters do begin
      sCommand += ' ' + s;
   end;
   item := cbCommands.ItemsEx.Insert(0);
   item.Caption := sCommand;
   FOutputs.Insert(0, TheOutput);
   cbCommands.ItemIndex := item.Index;
   mOutput.Lines.Text := TheOutput;
end;

end.
