unit NetworkLeprechaun.Wireguard.FrameTab;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   ComCtrls,
   Dialogs,
   Menus,
   Process,
   ActnList,
   OVM.ListView,
   Wireguard.Connections,
   NetworkLeprechaun.Wireguard.FormConfig,
   NetworkLeprechaun.Wireguard.FramePrerequisites;

type

   { TFrameWireguard }

   TFrameWireguard = class(TFrame)
      aImportFromKeyChain: TAction;
      aRefresh: TAction;
      aConnect: TAction;
      aDisconnect: TAction;
      aAdd: TAction;
      aEdit: TAction;
      aDelete: TAction;
      alActions: TActionList;
      FrameWireguardPrerequisites1: TFrameWireguardPrerequisites;
      lvConnections: TListView;
      miWireguardDelete: TMenuItem;
      pmiWireguardAdd: TMenuItem;
      pmiWireguardEdit: TMenuItem;
      popupList: TPopupMenu;
      procedure aAddExecute({%H-}Sender: TObject);
      procedure aConnectExecute({%H-}Sender: TObject);
      procedure aDeleteExecute({%H-}Sender: TObject);
      procedure aDisconnectExecute({%H-}Sender: TObject);
      procedure aEditExecute({%H-}Sender: TObject);
      procedure aImportFromKeyChainExecute({%H-}Sender: TObject);
      procedure aRefreshExecute({%H-}Sender: TObject);
      procedure lvConnectionsSelectItem({%H-}Sender: TObject; Item: TListItem; Selected: boolean);
   private
      FListWireguard: TWireguardConnectionList;
      FOnRunCommand: TWireguardDisplayOutputEvent;
      procedure DisplayOutput(ACommand: TProcessString; AParameters: array of TProcessString; AnOutput: TProcessString);
   public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure UpdateList;
      property OnRunCommand: TWireguardDisplayOutputEvent read FOnRunCommand write FOnRunCommand;
   end;

implementation

{$R *.lfm}

{ TFrameWireguard }

procedure TFrameWireguard.aAddExecute(Sender: TObject);
var
   conn: TWireguardConnection;
begin
   conn := TWireguardConnection.Create;
   if AddWireguardConfig(conn) then begin
      FListWireguard.Add(conn);
      aRefresh.Execute;
   end else begin
      conn.Free;
   end;
end;

procedure TFrameWireguard.aConnectExecute(Sender: TObject);
var
   conn: TWireguardConnection;
begin
   if not Assigned(lvConnections.Selected) then begin
      Exit;
   end;
   conn := TWireguardConnection(lvConnections.Selected.Data);
   conn.Connect;

   FListWireguard.UpdateStatus;
   UpdateList;
end;

procedure TFrameWireguard.aDeleteExecute(Sender: TObject);
var
   conn: TWireguardConnection;
begin
   if not Assigned(lvConnections.Selected) then begin
      Exit;
   end;
   conn := TWireguardConnection(lvConnections.Selected.Data);
   if (mrYes = MessageDlg('Deleting Wireguard configuration file...', 'Are you sure you want to delete this configuration?' +
      #13#10 + conn.Filename, mtConfirmation, [mbYes, mbNo], 0)) then begin
      DeleteFile(conn.Filename);
      aRefresh.Execute;
   end;
end;

procedure TFrameWireguard.aDisconnectExecute(Sender: TObject);
var
   conn: TWireguardConnection;
begin
   if not Assigned(lvConnections.Selected) then begin
      Exit;
   end;
   conn := TWireguardConnection(lvConnections.Selected.Data);
   conn.Disconnect;
   UpdateList;
end;

procedure TFrameWireguard.aEditExecute(Sender: TObject);
begin
   EditWireguardConfig(TWireguardConnection(lvConnections.Selected.Data));
   aRefresh.Execute;
end;

procedure TFrameWireguard.aImportFromKeyChainExecute(Sender: TObject);
begin
   FrameWireguardPrerequisites1.FrameStatusWireguardConfigurations1.TakeAction;
end;

procedure TFrameWireguard.aRefreshExecute(Sender: TObject);
var
   tab: TTabSheet;
begin
   FListWireguard.UpdateStatus;
   UpdateList;
   if (Self.Parent is TTabSheet) then begin
      tab := TTabSheet(Self.Parent);
      if (tab <> tab.PageControl.ActivePage) then begin
         tab.Show;
      end;
   end;
end;

procedure TFrameWireguard.lvConnectionsSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
var
   conn: TWireguardConnection;
begin
   aConnect.Enabled := Selected;
   aDisconnect.Enabled := Selected;
   aEdit.Enabled := Selected;
   aDelete.Enabled := Selected;
   if Selected then begin
      conn := TWireguardConnection(Item.Data);
      aConnect.Enabled := not conn.Connected;
      aDisconnect.Enabled := conn.Connected;
   end;
end;

procedure TFrameWireguard.DisplayOutput(ACommand: TProcessString; AParameters: array of TProcessString; AnOutput: TProcessString);
begin
   if Assigned(FOnRunCommand) then begin
      FOnRunCommand(ACommand, AParameters, AnOutput);
   end;
end;

constructor TFrameWireguard.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
   FListWireguard := TWireguardConnectionList.Create;
   FListWireguard.OnRunCommand := DisplayOutput;
end;

destructor TFrameWireguard.Destroy;
begin
   FListWireguard.Free;
   inherited Destroy;
end;

procedure TFrameWireguard.UpdateList;
begin
   FrameWireguardPrerequisites1.FrameStatusWireguardConfigurations1.Connections := FListWireguard;
   FListWireguard.ReadConfigurationFiles;
   FListWireguard.UpdateStatus;
   FrameWireguardPrerequisites1.UpdateFrames;
   lvConnections.DisplayItems<TWireguardConnection>(FListWireguard);
end;

end.
