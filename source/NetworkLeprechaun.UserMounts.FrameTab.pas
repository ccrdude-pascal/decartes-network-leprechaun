unit NetworkLeprechaun.UserMounts.FrameTab;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   ActnList,
   Menus,
   Process,
   ComCtrls,
   OVM.ListView,
   Firefly.MacOS.UserMounts,
   NetworkLeprechaun.UserMounts.FramePrerequisites,
   NetworkLeprechaun.UserMounts.FormConfig;

type

   { TFrameUserMounts }

   TFrameUserMounts = class(TFrame)
      aAdd: TAction;
      aConnect: TAction;
      aDelete: TAction;
      aDisconnect: TAction;
      aEdit: TAction;
      alActions: TActionList;
      aRefresh: TAction;
      FrameUserMountsPrerequisites1: TFrameUserMountsPrerequisites;
      lvMounts: TListView;
      miWireguardDelete: TMenuItem;
      pmiWireguardAdd: TMenuItem;
      pmiWireguardEdit: TMenuItem;
      popupList: TPopupMenu;
      procedure aAddExecute({%H-}Sender: TObject);
      procedure aConnectExecute(Sender: TObject);
      procedure aDeleteExecute({%H-}Sender: TObject);
      procedure aDisconnectExecute(Sender: TObject);
      procedure aEditExecute(Sender: TObject);
      procedure aRefreshExecute({%H-}Sender: TObject);
      procedure lvMountsSelectItem({%H-}Sender: TObject; Item: TListItem; Selected: boolean);
   private
      FUserMounts: TMacOSMountPoints;
      FOnRunCommand: TOnProcessActionEvent;
      procedure DoProcessAction(const AnExecutableName: TProcessString; const TheParameters: array of TProcessString;
         const TheOutput, TheErrors: string; TheExitStatus: integer);
   public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure UpdateList;
      property OnRunCommand: TOnProcessActionEvent read FOnRunCommand write FOnRunCommand;
   end;

implementation

{$R *.lfm}

uses
   Dialogs;

{ TFrameUserMounts }

procedure TFrameUserMounts.aRefreshExecute(Sender: TObject);
begin
   UpdateList;
end;

procedure TFrameUserMounts.lvMountsSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
var
   conn: TMacOSMountPoint;
begin
   aConnect.Enabled := Selected;
   aDisconnect.Enabled := Selected;
   aEdit.Enabled := Selected;
   aDelete.Enabled := Selected;
   if Selected then begin
      conn := TMacOSMountPoint(Item.Data);
      aConnect.Enabled := not conn.IsMounted;
      aDisconnect.Enabled := conn.IsMounted;
   end;
end;

procedure TFrameUserMounts.DoProcessAction(const AnExecutableName: TProcessString; const TheParameters: array of TProcessString;
   const TheOutput, TheErrors: string; TheExitStatus: integer);
begin
   if Assigned(FOnRunCommand) then begin
      FOnRunCommand(AnExecutableName, TheParameters, TheOutput, TheErrors, TheExitStatus);
   end;
end;

procedure TFrameUserMounts.aConnectExecute(Sender: TObject);
var
   conn: TMacOSMountPoint;
begin
   if not Assigned(lvMounts.Selected) then begin
      Exit;
   end;
   conn := TMacOSMountPoint(lvMounts.Selected.Data);
   conn.Mount(Sender);
   UpdateList;
end;

procedure TFrameUserMounts.aDeleteExecute(Sender: TObject);
var
   conn: TMacOSMountPoint;
begin
   conn := TMacOSMountPoint(lvMounts.Selected.Data);
   if (mrYes = MessageDlg('Deleting sshfs mount point...', 'Are you sure you want to delete this configuration?' + #13#10 +
      conn.FolderName, mtConfirmation, [mbYes, mbNo], 0)) then begin
      FUserMounts.Delete(FUserMounts.IndexOf(conn));
      FUserMounts.SaveToIniFile;
   end;
end;

procedure TFrameUserMounts.aAddExecute(Sender: TObject);
var
   conn: TMacOSMountPoint;
begin
   conn := TMacOSMountPoint.Create;
   if AddUserMountConfig(conn) then begin
      FUserMounts.Add(conn);
      FUserMounts.SaveToIniFile;
      aRefresh.Execute;
   end else begin
      conn.Free;
   end;
end;

procedure TFrameUserMounts.aDisconnectExecute(Sender: TObject);
var
   conn: TMacOSMountPoint;
begin
   if not Assigned(lvMounts.Selected) then begin
      Exit;
   end;
   conn := TMacOSMountPoint(lvMounts.Selected.Data);
   conn.Unmount(Sender);
   UpdateList;
end;

procedure TFrameUserMounts.aEditExecute(Sender: TObject);
begin
   if EditUserMountConfig(TMacOSMountPoint(lvMounts.Selected.Data)) then begin
      FUserMounts.SaveToIniFile;
      aRefresh.Execute;
   end;
end;

constructor TFrameUserMounts.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
   FUserMounts := TMacOSMountPoints.Create;
   FUserMounts.LoadFromIniFile();
   FUserMounts.OnAction := DoProcessAction;
end;

destructor TFrameUserMounts.Destroy;
begin
   inherited Destroy;
end;

procedure TFrameUserMounts.UpdateList;
begin
   FUserMounts.LoadFromIniFile();
   FUserMounts.UpdateStatus;
   FrameUserMountsPrerequisites1.UpdateFrames;
   lvMounts.DisplayItems<TMacOSMountPoint>(FUserMounts);
end;

end.
