{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Listing and controlling WireGuard connections.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-01-24  pk   2h  Wrote code to retrieve WireGuard entries.
// *****************************************************************************
   )
}
unit Wireguard.Connections;

{$mode Delphi}{$H+}

{$IFDEF Darwin}
{$linkframework Security}
{$modeswitch objectivec1 on}
{$ENDIF Darwin}

interface

uses
   Classes,
   SysUtils,
   Process,
   Firefly.Process,
   OVM.Attributes,
   OVM.ListView.Attributes,
   Generics.Collections;

type
   TWireguardDisplayOutputEvent = procedure(ACommand: TProcessString; AParameters: array of TProcessString; AnOutput: TProcessString) of object;

   TWireguardConnectionList = class;

   { TWireguardConnection }

   TWireguardConnection = class
   private
      FConnected: boolean;
      FFilename: string;
      FPeerEndPoint: string;
      FPeerPublicKey: string;
      FStatusOutput: TStringList;
      FTitle: string;
      FList: TWireguardConnectionList;
      function GetConnectedText: WideString;
      procedure SetFilename(AValue: string);
   protected
      function RunWireguardCommand(const exename: TProcessString; const commands: array of TProcessString; out outputstring: string;
         Options: TProcessOptions = []; SWOptions: TShowWindowOptions = swoNone): boolean;
   public
      constructor Create;
      destructor Destroy; override;
      procedure Connect;
      procedure Disconnect;
      property StatusOutput: TStringList read FStatusOutput;
      property Filename: string read FFilename write SetFilename;
      property PeerPublicKey: string read FPeerPublicKey;
   published
      property Title: string read FTitle;
      property PeerEndPoint: string read FPeerEndPoint;
      [AListViewColumnDetails('Connected')]
      property ConnectedText: WideString read GetConnectedText;
      [AOVMBooleanImageIndex(0, -1)]
      property Connected: boolean read FConnected;
      // property CurrentInterface: string read FCurrentInterface;
   end;

   { TWireguardConnectionList }

   TWireguardConnectionList = class(TObjectList<TWireguardConnection>)
   private
      FOnRunCommand: TWireguardDisplayOutputEvent;
      function RunWireguardCommand(const exename: TProcessString; const commands: array of TProcessString; out outputstring: string;
         Options: TProcessOptions = []; SWOptions: TShowWindowOptions = swoNone): boolean;
   public
      procedure ReadConfigurationFiles;
      procedure UpdateStatus;
      function Find(APeerEndPoint: string; APeerPublicKey: string): TWireguardConnection; overload;
      function Find(APeerEndPoint: string; APeerPublicKey: string; out AConnection: TWireguardConnection): boolean; overload;
      property OnRunCommand: TWireguardDisplayOutputEvent read FOnRunCommand write FOnRunCommand;
   end;

implementation

uses
   IniFiles,
   Dialogs,
   Firefly.MacOS.Elevation;

const
  {$IF DEFINED(MSWindows)}
  WireGuardExecutableFilenameWG = 'wg.exe';
  {$ELSE}
  WireGuardExecutableFilenameWG = '/usr/local/bin/wg';
  WireGuardExecutableFilenameWGQuick = '/usr/local/bin/wg-quick';
  WireGuardConfigurationFilePath = '/usr/local/etc/wireguard/';
  {$IFEND}

{ TWireguardConnection }

procedure TWireguardConnection.SetFilename(AValue: string);
var
   mif: TMemIniFile;
begin
   FFilename := AValue;
   if 0 = Length(AValue) then begin
      Exit;
   end;
   FTitle := ExtractFileName(FFilename);
   FTitle := Copy(FTitle, 1, Length(FTitle) - Length(ExtractFileExt(FTitle)));
   mif := TMemIniFile.Create(FFilename);
   try
      FPeerEndPoint := mif.ReadString('Peer', 'Endpoint', '');
      FPeerPublicKey := mif.ReadString('Peer', 'PublicKey', '');
   finally
      mif.Free;
   end;
  (*
  [Interface]
  PrivateKey = ...
  Address = 192.168.9.212/24
  DNS = 192.168.178.41, 192.168.9.1, fritz.box

  [Peer]
  PresharedKey = ...
  AllowedIPs = 192.168.9.0/24
  PersistentKeepalive = 25
  *)
end;

function TWireguardConnection.RunWireguardCommand(const exename: TProcessString; const commands: array of TProcessString;
   out outputstring: string; Options: TProcessOptions; SWOptions: TShowWindowOptions): boolean;
begin
   Result := FList.RunWireguardCommand(exename, commands, outputstring, Options, SWOptions);
end;

function TWireguardConnection.GetConnectedText: WideString;
begin
   if FConnected then begin
      Result := 'connected';
   end else begin
      Result := '-';
   end;
end;

constructor TWireguardConnection.Create;
begin
   FConnected := False;
   FStatusOutput := TStringList.Create;
end;

destructor TWireguardConnection.Destroy;
begin
   FStatusOutput.Free;
   inherited Destroy;
end;

procedure TWireguardConnection.Connect;
var
   s: string;
begin
   RunWireguardCommand(WireGuardExecutableFilenameWGQuick, ['up', Self.Title], s);
end;

procedure TWireguardConnection.Disconnect;
var
   s: string;
begin
   RunWireguardCommand(WireGuardExecutableFilenameWGQuick, ['down', Self.Title], s);
end;

{ TWireguardConnectionList }

function TWireguardConnectionList.RunWireguardCommand(const exename: TProcessString; const commands: array of TProcessString;
   out outputstring: string; Options: TProcessOptions; SWOptions: TShowWindowOptions): boolean;
begin
   Result := RunLoggedCommand(exename, commands, outputstring, Options, SWOptions);
   if Assigned(FOnRunCommand) then begin
      FOnRunCommand(exename, commands, outputstring);
   end;
end;

procedure TWireguardConnectionList.ReadConfigurationFiles;
var
   i: integer;
   sr: TSearchRec;
   sPath: string;
   conn: TWireguardConnection;
begin
   Self.Clear;
   sPath := WireGuardConfigurationFilePath;
   i := FindFirst(sPath + '*.conf', faAnyFile, sr);
   try
      while (i = 0) do begin
         conn := TWireguardConnection.Create;
         conn.Filename := sPath + sr.Name;
         conn.FList := Self;
         Self.Add(conn);
         i := FindNext(sr);
      end;
   finally
      FindClose(sr);
   end;
end;

procedure TWireguardConnectionList.UpdateStatus;
var
   conn: TWireguardConnection;
   s: string;
   sl: TStringList;
   i: integer;
begin
  (*
  interface: utun7
    public key: ,,,
    private key: (hidden)
    listening port: 4711

  peer: ...
    preshared key: (hidden)
    endpoint: e.f.g.h:4711
    allowed ips: a.b.c.d/24
    latest handshake: 35 seconds ago
    transfer: 14.39 KiB received, 9.13 KiB sent
    persistent keepalive: every 25 seconds

  *)
   RunWireguardCommand(WireGuardExecutableFilenameWG, ['show'], s);
   sl := TStringList.Create;
   try
      sl.Text := s;
      for conn in Self do begin
         i := sl.IndexOf('peer: ' + conn.PeerPublicKey);
         conn.FConnected := (i > -1);
         conn.StatusOutput.Clear;
         if conn.FConnected then begin
            conn.StatusOutput.Add(sl[i]);
         end;
      end;
   finally
      sl.Free;
   end;
end;

function TWireguardConnectionList.Find(APeerEndPoint: string; APeerPublicKey: string): TWireguardConnection;
var
   e: TWireguardConnection;
begin
   Result := nil;
   for e in Self do begin
      if SameText(APeerEndPoint, e.PeerEndPoint) and SameText(APeerPublicKey, e.PeerPublicKey) then begin
         Result := e;
         Exit;
      end;
   end;
end;

function TWireguardConnectionList.Find(APeerEndPoint: string; APeerPublicKey: string; out AConnection: TWireguardConnection): boolean;
var
   e: TWireguardConnection;
begin
   Result := False;
   AConnection := nil;
   for e in Self do begin
      if SameText(APeerEndPoint, e.PeerEndPoint) and SameText(APeerPublicKey, e.PeerPublicKey) then begin
         Result := True;
         AConnection := e;
         Exit;
      end;
   end;
end;

end.
