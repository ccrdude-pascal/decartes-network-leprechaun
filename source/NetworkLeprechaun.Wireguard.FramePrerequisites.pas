unit NetworkLeprechaun.Wireguard.FramePrerequisites;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   NetworkLeprechaun.FrameStatus,
   NetworkLeprechaun.Wireguard.FrameStatusHomebrew,
   NetworkLeprechaun.Wireguard.FrameStatusWG,
   NetworkLeprechaun.Wireguard.FrameStatusConfigurations;

type

   { TFrameWireguardPrerequisites }

   TFrameWireguardPrerequisites = class(TFrame)
      FrameStatusHomebrew1: TFrameStatusHomebrew;
      FrameStatusWireguard1: TFrameStatusWireguard;
      FrameStatusWireguardConfigurations1: TFrameStatusWireguardConfigurations;
      sbStati: TScrollBox;
   private
   public
      procedure UpdateFrames;
   end;

implementation

{$R *.lfm}

uses
   Dialogs;

   { TFrameWireguardPrerequisites }


procedure TFrameWireguardPrerequisites.UpdateFrames;
begin
   FrameStatusHomebrew1.UpdateStatus;
   FrameStatusWireguard1.UpdateStatus;
   FrameStatusWireguardConfigurations1.UpdateStatus;
   if (FrameStatusHomebrew1.StatusWorking) and (FrameStatusWireguard1.StatusWorking) and (FrameStatusWireguardConfigurations1.StatusWorking) then begin
      Self.Width := FrameStatusHomebrew1.imgStatus.Width + (FrameStatusHomebrew1.imgStatus.BorderSpacing.Around * 2) +
         (sbStati.ChildSizing.LeftRightSpacing * 2);
   end;
end;

end.
