unit NetworkLeprechaun.UserMounts.FrameSSHFS;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   NetworkLeprechaun.FrameStatus;

type

   { TFrameStatusSSHFS }

   TFrameStatusSSHFS = class(TFrameStatus)
   private

   public
      function StatusWorking: boolean; override;
      procedure UpdateStatus; override;
      procedure TakeAction; override;
   end;

implementation

{$R *.lfm}

uses
   LCLIntf;

{ TFrameStatusSSHFS }

function TFrameStatusSSHFS.StatusWorking: boolean;
begin
   Result := FileExists('/usr/local/bin/sshfs');
end;

procedure TFrameStatusSSHFS.UpdateStatus;
begin
   inherited UpdateStatus;
   if StatusWorking then begin
      labelStatus.Caption := 'SSHFS installed.';
   end else begin
      labelStatus.Caption := 'SSHFS missing!';
      bnAction.Caption := 'Install SSHFS';
   end;
end;

procedure TFrameStatusSSHFS.TakeAction;
begin
   OpenURL('https://osxfuse.github.io/');
end;

end.
