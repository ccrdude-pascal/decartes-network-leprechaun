unit NetworkLeprechaun.UserMounts.FramePrerequisites;

{$mode ObjFPC}{$H+}

interface

uses
   Classes, SysUtils, Forms, Controls,
   NetworkLeprechaun.UserMounts.FrameLibFuse,
   NetworkLeprechaun.UserMounts.FrameSSHFS;

type

   { TFrameUserMountsPrerequisites }

   TFrameUserMountsPrerequisites = class(TFrame)
      FrameStatusLibFuse1: TFrameStatusLibFuse;
      FrameStatusSSHFS1: TFrameStatusSSHFS;
      sbStati: TScrollBox;
   private

   public
     procedure UpdateFrames;
   end;

implementation

{$R *.lfm}

{ TFrameUserMountsPrerequisites }

procedure TFrameUserMountsPrerequisites.UpdateFrames;
begin
  FrameStatusLibFuse1.UpdateStatus;
  FrameStatusSSHFS1.UpdateStatus;
  if (FrameStatusLibFuse1.StatusWorking) and (FrameStatusSSHFS1.StatusWorking) then begin
     Self.Width := FrameStatusLibFuse1.imgStatus.Width + (FrameStatusLibFuse1.imgStatus.BorderSpacing.Around * 2) +
        (sbStati.ChildSizing.LeftRightSpacing * 2);
  end;
end;

end.

