// @see https://www.tweaking4all.com/software-development/lazarus-development/macos-smjobbless-elevated-privileges-lazarus-pascal/

unit NetworkLeprechaun.FormMain;

{$mode Delphi}{$H+}

// if macOS
{$linkframework Security}
{$modeswitch objectivec1 on}


interface

uses
   Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ActnList,
   ExtCtrls, StdCtrls, Menus,
   MacOSAll,
   Wireguard.KeyChain,
   NetworkLeprechaun.Wireguard.FrameTab,
   NetworkLeprechaun.UserMounts.FrameTab, NetworkLeprechaun.FrameTests;

type

   { TFormNetworkLeprechaun }

   TFormNetworkLeprechaun = class(TForm)
      FrameTerminalCommandOutput1: TFrameTerminalCommandOutput;
      FrameUserMounts1: TFrameUserMounts;
      FrameWireguard1: TFrameWireguard;
      ilStandard: TImageList;
      ilDisabled: TImageList;
      ilLists: TImageList;
      labelToggle: TLabel;
      memoDebug: TMemo;
      MenuItem2: TMenuItem;
      menuMain: TMainMenu;
      MenuItem1: TMenuItem;
      miWireguardRefresh: TMenuItem;
      MenuItem3: TMenuItem;
      miSSHFSRefresh: TMenuItem;
      panelCommandLineOutput: TPanel;
      pcMain: TPageControl;
      panelToolbarWireguard: TPanel;
      Separator1: TMenuItem;
      tabAbout: TTabSheet;
      tabSSHFS: TTabSheet;
      tabWireguard: TTabSheet;
      toolbarWireguard: TToolBar;
      tbRefresh: TToolButton;
      tbConnect: TToolButton;
      tbDisconnect: TToolButton;
      procedure FormCreate({%H-}Sender: TObject);
      procedure FormShow({%H-}Sender: TObject);
      procedure labelToggleClick({%H-}Sender: TObject);
      procedure MenuItem2Click({%H-}Sender: TObject);
      procedure miSSHFSRefreshClick({%H-}Sender: TObject);
      procedure miWireguardRefreshClick({%H-}Sender: TObject);
      procedure tabAboutShow({%H-}Sender: TObject);
      procedure tabSSHFSShow({%H-}Sender: TObject);
      procedure tabWireguardShow({%H-}Sender: TObject);
   private
   public
   end;

var
   FormNetworkLeprechaun: TFormNetworkLeprechaun;

implementation

{$R *.lfm}

uses
   Firefly.MacOS.Elevation;

   { TFormNetworkLeprechaun }

procedure TFormNetworkLeprechaun.FormCreate(Sender: TObject);
begin
   TMacOSElevation.ElevateIfNot();
   FrameWireguard1.OnRunCommand := FrameTerminalCommandOutput1.DisplayOutput;
   FrameUserMounts1.OnRunCommand := FrameTerminalCommandOutput1.DisplayOutputEx;
end;

procedure TFormNetworkLeprechaun.FormShow(Sender: TObject);
var
   s: OSStatus;
   ver: uint32;
begin
   tabWireguard.Show;
   Initialize(ver);
   s := KCGetKeychainManagerVersion(ver);
   memoDebug.Lines.Add('');
   memoDebug.Lines.Add(Format('KCGetKeychainManagerVersion(returnVers = 0x%s) = %d', [IntToHex(ver, 8), s]));
end;

procedure TFormNetworkLeprechaun.labelToggleClick(Sender: TObject);
begin
   FrameTerminalCommandOutput1.Visible := not FrameTerminalCommandOutput1.Visible;
end;

procedure TFormNetworkLeprechaun.MenuItem2Click(Sender: TObject);
begin
   FrameWireguard1.aImportFromKeyChain.Execute;;
end;

procedure TFormNetworkLeprechaun.miSSHFSRefreshClick(Sender: TObject);
begin
   FrameUserMounts1.aRefresh.Execute;
end;

procedure TFormNetworkLeprechaun.miWireguardRefreshClick(Sender: TObject);
begin
   FrameWireguard1.aRefresh.Execute;
end;

procedure TFormNetworkLeprechaun.tabAboutShow(Sender: TObject);
begin
end;

procedure TFormNetworkLeprechaun.tabSSHFSShow(Sender: TObject);
begin
   tbRefresh.Action := FrameUserMounts1.aRefresh;
   tbConnect.Action := FrameUserMounts1.aConnect;
   tbDisconnect.Action := FrameUserMounts1.aDisconnect;
   FrameUserMounts1.UpdateList;
end;

procedure TFormNetworkLeprechaun.tabWireguardShow(Sender: TObject);
begin
   tbRefresh.Action := FrameWireguard1.aRefresh;
   tbConnect.Action := FrameWireguard1.aConnect;
   tbDisconnect.Action := FrameWireguard1.aDisconnect;
   FrameWireguard1.UpdateList;
end;

end.
