unit NetworkLeprechaun.Wireguard.FormConfig;

{$mode ObjFPC}{$H+}

interface

uses
   Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ButtonPanel,
   SynEdit, SynHighlighterIni, Wireguard.Connections;

type

   { TFormWireguardEdit }

   TFormWireguardEdit = class(TForm)
      ButtonPanel1: TButtonPanel;
      editTitle: TEdit;
      editConfig: TSynEdit;
      labelTitle: TLabel;
      labelConfiguration: TLabel;
      SynIniSyn1: TSynIniSyn;
      procedure editTitleChange({%H-}Sender: TObject);
   private

   public
      function Add(AConfig: TWireguardConnection): boolean;
      function Edit(AConfig: TWireguardConnection): boolean;
   end;

function AddWireguardConfig(AConfig: TWireguardConnection): boolean;
function EditWireguardConfig(AConfig: TWireguardConnection): boolean;

implementation

function AddWireguardConfig(AConfig: TWireguardConnection): boolean;
var
   form: TFormWireguardEdit;
begin
   form := TFormWireguardEdit.Create(nil);
   try
      Result := form.Add(AConfig);
   finally
      form.Free;
   end;
end;

function EditWireguardConfig(AConfig: TWireguardConnection): boolean;
var
   form: TFormWireguardEdit;
begin
   form := TFormWireguardEdit.Create(nil);
   try
      Result := form.Edit(AConfig);
   finally
      form.Free;
   end;
end;

{$R *.lfm}

{ TFormWireguardEdit }

procedure TFormWireguardEdit.editTitleChange(Sender: TObject);
begin
   ButtonPanel1.OKButton.Enabled := (Length(editTitle.Text) > 0);
end;

function TFormWireguardEdit.Add(AConfig: TWireguardConnection): boolean;
var
   sFilename: string;
begin
   ButtonPanel1.OKButton.Enabled := False;
   editTitle.ReadOnly := False;
   editTitle.Text := AConfig.Title;
   editConfig.Lines.Clear;
   Result := (mrOk = ShowModal);
   if Result then begin
      sFilename := '/usr/local/etc/wireguard/' + editTitle.Text + '.conf';
      editConfig.Lines.SaveToFile(sFilename);
      AConfig.Filename := sFilename;
   end;
end;

function TFormWireguardEdit.Edit(AConfig: TWireguardConnection): boolean;
begin
   ButtonPanel1.OKButton.Enabled := True;
   editTitle.ReadOnly := True;
   editTitle.Text := AConfig.Title;
   editConfig.Lines.LoadFromFile(AConfig.Filename);
   Result := (mrOk = ShowModal);
   if Result then begin
      editConfig.Lines.SaveToFile(AConfig.Filename);
   end;
end;

end.
