# Network Leprechaun

## Purpose

### Wireguard Control

The Wireguard Graphical User Interface on macOS does not allow to establish more
than one VPN connection at a time, which was not sufficient for my purpose.

A working alternative is to use the command line version of Wireguard. After a
while, I decided to wrap a small user interface around it to simplify
connections in my daily use, which became this project.

### SSH-FS Control

I work on websites a lot, and after using Wireguard to connect to test
installations, my next step usually is to mount a wbsite file location using
sshfs. This feature was already part of a separate tool and merged into this
to have things in one place.

## TODO List

- [x] Move tabs to separate frames
- [x] Execution panel at bottom

### Wireguard

- [x] Edit Wireguard Config
- [x] Add Wireguard Config
- [x] Delete Wireguard Config
- [x] Add Homebrew instructions
- [x] Add wg cli installation instructions
- [x] Prereq List.Items.Count = 0 ?
- [x] All Status Actions
- [x] Import from keychain?

### SSHFS

- [x] Load config
- [x] Display mount points
- [x] Mount/unmount
- [x] add
- [x] edit
- [x] delete
- [x] Prereq panel
- [x] Prereq /usr/local/bin/sshfs
- [x] Prereq libfuse
- [x] All Status Actions
- [x] mount as user instead of root

